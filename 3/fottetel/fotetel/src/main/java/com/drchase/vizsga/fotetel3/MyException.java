package com.drchase.vizsga.fotetel3;

class MyException extends Exception {
    public MyException(String s)    {
        super(s);
    }
}