package com.drchase.vizsga.fotetel3;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < args.length; i++)   {
            Teszt teszt = new Teszt();
            try{
                teszt.dobo(args[i]);
                System.out.println("Teszt \"" + args[i] + "\" nem váltott ki kivételt");
            }
            catch (Exception e)   {
                System.out.println("Teszt \"" + args[i] + "\" kiváltott egy kivételt: " +
                                    e.getClass() + "\n " + e.getMessage() + " üzenettel");
                e.printStackTrace();
            }
        }
    }
}