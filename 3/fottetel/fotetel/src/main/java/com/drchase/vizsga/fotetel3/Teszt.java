package com.drchase.vizsga.fotetel3;

public class Teszt    {
    void dobo(String s) throws MyException   {
        int i = 0;
        try {
            if (s.equals("osztas")) {
                i = i/i;
            }
            if (s.equals("null"))   {
                s = null;
                i = s.length();
            }
            if (s.equals("teszt")) 
                throw new MyException("Teszt üzenet");
        }
        finally {
            System.out.println("[dobo(\"" + s +"\") vége]");
        }
    }
}