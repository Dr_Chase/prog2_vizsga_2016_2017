package com.drchase.vizsga.fotetel2;

aspect NullaSzamlalo    {

    public static int numberOfZeros = 0;

    pointcut buildPoint(Byte myByte):
        call(void LZWTreeClass.addByte(Byte))
        && args(myByte);

    pointcut getDeviation():
        call(double LZWTreeClass.getDeviation());

    before(Byte myByte): buildPoint(myByte)  {
        if(myByte.intValue() == 0 )
            numberOfZeros++;
    }

    after(): getDeviation() {
        System.out.println("number of zero input bytes: " + numberOfZeros);
    }
}
/* // original works!
pointcut zeroBuildPoint(Object myObject,Node myNode):
        call(void Node.setLeftChild(Object )) 
        && target(myNode)
        && args(myObject);

before(Object myObject, Node myNode): zeroBuildPoint(myObject, myNode)  {
        numberOfZeros++;
    }
*/