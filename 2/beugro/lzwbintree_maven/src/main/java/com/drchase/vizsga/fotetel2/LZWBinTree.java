/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.drchase.vizsga.fotetel2;
import java.io.*;
/**
 *
 * @author Dr.Chase
 */
public class LZWBinTree {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Tests
//        Node<Integer> test = new Node<>(4);
//        test.leftChild = new Node<Integer>(2);
//        System.out.println(test.getElement());
//        System.out.println(test.hasLeftChild());
//        
//        Node<Integer> cloned = new Node<Integer>();
//        System.out.println(test.getElement());
//        System.out.println(cloned.getElement());

//        LZWTreeClass myTree = new LZWTreeClass();
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        //System.out.println(myTree.getRoot().getElement());
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)1);
        
//        //System.out.println(myTree.travellingNode.getElement());
//        
        try {
            BinTreeFileIO testing = new BinTreeFileIO();
            testing.BuildTreeForTest("ACT");
                            // ================================================================
            System.out.println("======================================================");
            //System.out.println(BinaryTree.leaves.size());
            System.out.println("Depth: \t\t" + testing.BinaryTree.getDepth());
            System.out.println("AvgLeafDepth: \t" + testing.BinaryTree.getAverageLeafDepth());
            System.out.println("Deviation: \t" + testing.BinaryTree.getDeviation());
            System.out.println("======================================================");
        }
        /*
        catch(FileNotFoundException ex) {
//            System.out.println(
//                "Unable to open file '" + 
//                fileName + "'"
                  ex.printStackTrace();
//            );                
        }
        catch(IOException ex) {
//            System.out.println(
//                "Error reading file '" 
//                + fileName + "'");                  
//             Or we could just do this: 
             ex.printStackTrace();
        }
        */
        catch (Exception e) {
            System.out.println("Unknown Error");
        }
    }
}
