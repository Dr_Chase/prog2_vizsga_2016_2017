package com.drchase.vizsga.fotetel8;

import java.util.*;
/**
 *
 * @author Dr.Chase
 */
public class Verem extends java.util.LinkedList{
    public Verem(Collection c)  {
        super(c);
    }
    public void push(Object o)  {
        addFirst(o);
    }
    public Object top() {
        return getFirst();
    }
    public Object pop() {
        return removeFirst();
    } 
}