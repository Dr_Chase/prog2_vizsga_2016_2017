package com.drchase.vizsga.fotetel1;

public class Alkalmazott    {
    //constants

    String nev;
    private int fizetes = 30000; // LEHET KEZDŐÉRTÉKET MEGADNI
    private int evesFizetes;
    int potklekok;
    int levonasok = fizetes / 4; // lehet már meglévő kezdőértékből számított kezdőértéket megadni
    final double ADOKULCS = 25;  // konstans javában, nagybetűs konvenció

    static int nyugdijKorhatar = 60;
    int eletkor;

    int nyelvekSzama;
    public int potlek() {
        return nyelvekSzama * 5000;
    }

    public int fizetesPotlekokkal(){
        return getFizetes() + potlek();
    }

    //konstruktorok
    public Alkalmazott()   {
        this.nev = null;
        evesFizetes = fizetes * 12;
    }
    public Alkalmazott(String nev, int fizetes) {
        this.nev = nev;
        this.fizetes = fizetes;
        this.evesFizetes = 12 * fizetes;
    }
    public Alkalmazott (String nev) {
        this(nev, 40000);
    }

    static void nyugdijKorhatarEmel()   {
        nyugdijKorhatar++;
    }
    int hatraVan()  {
        return nyugdijKorhatar - eletkor;
    }



    public int getFizetes() {
        return this.fizetes;
    }
    public int getEvesFizetes() {
        return this.evesFizetes;
    }

    public void setFizetes(int ujErtek)  {
        this.fizetes = ujErtek;
    }

    void ujFizetes(int osszeg)  {
        fizetes = osszeg;
        evesFizetes = 12 * osszeg;
    }

    void fizetestEmel(int novekmeny)    {
    fizetes += novekmeny;
    }
    // metódustúlterhelés
    void fizetestEmel() {
        fizetes += Constants.FIZETESEMELES1;
    }
    void fizetestEmel(Alkalmazott masik)   {
        if (kevesebbetKeresMint(masik))
            fizetes = masik.fizetes;
    }

    boolean tobbetKeresMint(Alkalmazott masik)  {
        return fizetes > masik.fizetes;
    }

    boolean kozepesJovedelmu(int minimum, int maximum)  {
        return minimum <= fizetes && fizetes <= maximum;
    }

    void fizetestDuplaz()   {
        fizetes *= 2;
    }

    void automatikusFizetesEmeles() {
        if(kozepesJovedelmu(Constants.MINIMUMFIZETES, Constants.MAXIMUMFIZETES))
            fizetestEmel(Constants.FIZETESEMELES1);
    }

    boolean kevesebbetKeresMint(Alkalmazott masik)  {
        return masik.tobbetKeresMint(this);
    }

    static int[] evesSzabadsag = new int [60];

    static  {
        for (int i = 0; i < 30; i++) evesSzabadsag[i] = 15;
        for (int i = 30; i < 40; i++) evesSzabadsag[i] = evesSzabadsag[i-1] + 1;
        for (int i = 40; i < 50; i++) evesSzabadsag[i] = evesSzabadsag[i-1] + 2;
        for (int i = 50; i < 60; i++) evesSzabadsag[i] = evesSzabadsag[i-1] + 1;
    }

    public int evesSzabadsag()  {
        return evesSzabadsag[eletkor < 60 ? eletkor : 59];
    }
    @Override
    public String toString()   {
        StringBuilder sb = new StringBuilder();
        sb.append("======================================\n");
        sb.append("Név: " + this.nev + "\n");
        sb.append("Fizetés: " + this.getFizetes() + "\n");
        sb.append("======================================\n");
        return sb.toString();
    }
}