package com.drchase.vizsga.fotetel1;

import com.drchase.vizsga.fotetel1.Alkalmazott;



public class Fonok extends Alkalmazott{
    final int MAXBEOSZT = 20;
    Alkalmazott[] beosztottak = new Alkalmazott[MAXBEOSZT];
    int beosztottakSzama = 0;

    //konstruktorok
    public Fonok(String nev, int fizetes)   {
        super(nev, fizetes);
    }

    public Fonok(String nev)    {
        this(nev, 100000);
    }
    public Fonok()  {
        this.nev = null;
    }

    public int potlek() {
        return super.potlek() + beosztottakSzama * 1000;
    }

    public void ujBeosztott(Alkalmazott b)  {
        beosztottak[beosztottakSzama++] = b;
    }
}