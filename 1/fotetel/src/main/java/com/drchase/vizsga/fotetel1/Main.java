package com.drchase.vizsga.fotetel1;

public class Main {
    public static void main(String[] args) {
        Alkalmazott a = new Alkalmazott();
        a.nev = "Mucsi Zoltán";
        a.setFizetes(50000);
        a.fizetestEmel(6300);
        System.out.println(a.nev + " fizetése: " + a.getFizetes());
        a.fizetestDuplaz();
        System.out.println(a.nev + " fizetése: " + a.getFizetes());

        Alkalmazott b = new Alkalmazott();
        b.nev = "Papp Laci";
        System.out.println(b.nev + " fizetése: " + b.getFizetes());
        b.fizetestEmel(a);
        System.out.println(b.nev + " fizetése: " + b.getFizetes());

        Alkalmazott c = new Alkalmazott("Jónás Lacika");
        Alkalmazott d = new Alkalmazott("Paksi Endre", 30000);
        System.out.println(c.toString());
        System.out.println(d.toString());

        //Polimorphizmus

        Alkalmazott aa = new Fonok("Kovács Gyula");
        int nPotlek = aa.potlek();
        int nFizetesPotlekokkal = aa.fizetesPotlekokkal();
    }
}