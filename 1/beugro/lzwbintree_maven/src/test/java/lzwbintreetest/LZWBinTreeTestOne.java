package lzwbintreetest;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.Assert.*;

import lzwbintree.BinTreeFileIO;


public class LZWBinTreeTestOne {
	@Test
	public void Case1TestOfSimple_Input() {	/*failure message string, expected, actual */ 
		BinTreeFileIO myBinTreeHandler;
		myBinTreeHandler = new BinTreeFileIO();
		myBinTreeHandler.BuildTreeForTest("ACT");
		Assert.assertEquals("FAILED! - Test case 1 ===> getDepth()", 3, myBinTreeHandler.BinaryTree.getDepth()); //4 is correct
		Assert.assertEquals("FAILED! - Test case 1 ===> getAverageLeafDepth()", 3.0, myBinTreeHandler.BinaryTree.getAverageLeafDepth() , 0.01);
		Assert.assertEquals("FAILED! - Test case 1 ===> getDeviation()", 0.0, myBinTreeHandler.BinaryTree.getDeviation(), 0.01);
	} // 3.009
}
