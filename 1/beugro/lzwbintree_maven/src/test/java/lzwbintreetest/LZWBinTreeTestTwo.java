package lzwbintreetest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;




import java.nio.file.*;

import lzwbintree.BinTreeFileIO;



public class LZWBinTreeTestTwo {
	
	static BinTreeFileIO myBinTreeHandler;
	static String lineSeparator;
	static String fileSeparator;
	static boolean shallWeRunTheTest;  

	@BeforeClass
	public static void setUpTestClass()	{
		lineSeparator = System.getProperty("line.separator");
		fileSeparator = File.separator;
		Path testFilePath;
		try{
			shallWeRunTheTest = true;
			testFilePath = Paths.get("./TestCase2.txt");
			File testFileInstance = testFilePath.toFile();
			myBinTreeHandler = new BinTreeFileIO(testFileInstance);
		} catch(FileNotFoundException e)	{
			shallWeRunTheTest = false;
			e.printStackTrace();
		} catch(IOException e)	{
			shallWeRunTheTest = false;
			e.printStackTrace();
		} catch(InvalidPathException e)	{
			shallWeRunTheTest = false;
			e.printStackTrace();
			System.err.println("Can't find TestCaseTwo.txt");
		} catch(Exception e)	{
			shallWeRunTheTest = false;
			e.printStackTrace();
		}
		
	}

	@Test
	public void Case2TestOfFileInput_Depth() {	/*failure message string, expected, actual */ 
		org.junit.Assume.assumeTrue(shallWeRunTheTest);
		Assert.assertEquals("FAILED! - Test case 1 ===> getDepth()", 4, myBinTreeHandler.BinaryTree.getDepth()); //4 is correct
	}

	@Test
	public void Case2TestOfFileInput_AvgLeafDepth() {	/*failure message string, expected, actual, hibahatar */
		org.junit.Assume.assumeTrue(shallWeRunTheTest);
		Assert.assertEquals("FAILED! - Test case 1 ===> getAverageLeafDepth()", 2.8, myBinTreeHandler.BinaryTree.getAverageLeafDepth() , 0.01);
	}	//2.8

	@Test
	public void Case2TestOfFileInput_Deviation() {	/*failure message string, expected, actual, hibahatar */
		org.junit.Assume.assumeTrue(shallWeRunTheTest);
		Assert.assertEquals("FAILED! - Test case 1 ===> getDeviation()", 0.83666002, myBinTreeHandler.BinaryTree.getDeviation(), 0.01);
	}
	
	@AfterClass
	public static void Case2cleanUp() {
	    myBinTreeHandler.BinaryTree = null;
		myBinTreeHandler = null;
	}
	
}
