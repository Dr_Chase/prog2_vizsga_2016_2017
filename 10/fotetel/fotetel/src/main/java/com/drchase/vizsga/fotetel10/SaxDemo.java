package com.drchase.vizsga.fotetel10;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;
import java.util.jar.Attributes;

public class SaxDemo extends DefaultHandler {
    boolean isFamilyName = false;
    public void startElement ( String namespaceUri, String localName, String rawName, Attributes atts ) throws SAXException {
        if (localName.equals("családnév") ) {
            isFamilyName = true;
        }
    }
    public void characters ( char[] ch, int start, int len )    {
        if (isFamilyName )  {
            String s = new String( ch, start, len );
            System.out.println( s );
        }
        isFamilyName = false;
    }

}