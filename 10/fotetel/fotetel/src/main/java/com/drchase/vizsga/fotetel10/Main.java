package com.drchase.vizsga.fotetel10;

import javax.xml.parsers.SAXParser;
import org.xml.sax.*;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import java.nio.file.Paths;
import java.nio.file.Path;

import java.util.*;
import java.io.*;

public class Main {

    private static String convertToFileURL(String filename) {
        String path = new File(filename).getAbsolutePath();
        if (File.separatorChar != '/') {
            path = path.replace(File.separatorChar, '/');
        }

        if (!path.startsWith("/")) {
            path = "/" + path;
        }
        return "file:" + path;
    }
    public static void main(String[] args) {
        try {
            boolean isValidating = false;
            boolean isValidatingSchema = false;
            String xmlURI = "";
            if (( args.length == 2 ) && ( args[0].equals("-v") ))   {
                isValidating = true;
                xmlURI = args[1];
            }
            else if (( args.length == 2 ) && (args[0].equals("-s") ))   {
                isValidating = true;
                isValidatingSchema = true;
                xmlURI = args[1];
            }
            else if (args.length == 1 )     {
                xmlURI = args[0];
                //System.out.println(xmlURI);
            }
            else    {
                System.err.println("Használat: java SaxDemo [-v|-s] XML_URI.");
                System.err.println("    -v: bekapcsolja az érvényesítést.");
                System.err.println("    -s: bekapcsolja az az érvényesítést XML Sémákkal.");
                System.exit(1);
            }
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setValidating(isValidating);        // true az alapértelmezés
            spf.setNamespaceAware( true );          // bekapcsolja a névtartományokat
            SAXParser saxParser = spf.newSAXParser();
            if ( isValidatingSchema )   {
                String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/" + "properties/schemaLanguage";
                String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
                saxParser.setProperty(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            }
            XMLReader parser = saxParser.getXMLReader();
            //ContentHandler contentHandler = new SaxDemo();
            ContentHandler contentHandler = new SAXLocalNameCount();
            parser.setContentHandler ( contentHandler );
            //mycode
            Path myFile = Paths.get(xmlURI);
            //System.out.println(myFile.toUri());
            //parser.parse ( xmlURI );
            //parser.parse ( myFile.toUri().toString() );S
            parser.parse ( myFile.toUri().toString());
            //System.out.println(xmlURI);
            //System.out.println(convertToFileURL(xmlURI));
            
        }
        catch (SAXException se )    {
            System.err.println( "SAX Hiba:" +se);
        }
        catch (java.io.IOException ioe) {
            System.err.println("IO Hiba:" + ioe);
        }
        catch (Exception e) {
            System.err.println( "Hiba:" + e);
        }
    }
}