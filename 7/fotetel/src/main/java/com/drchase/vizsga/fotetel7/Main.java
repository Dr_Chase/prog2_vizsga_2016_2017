package com.drchase.vizsga.fotetel7;

public class Main {
    public static void main(String[] args) {
        LinkedList<Byte> byteList = new LinkedList<Byte>();
        byteList.add(new Byte("0"));
        byteList.add(new Byte("1"));
        Byte x = byteList.iterator().next();
        LinkedList<String> stringList = new LinkedList<String>();
        stringList.add("zero");
        stringList.add("one");
        String myString = stringList.iterator().next();
        LinkedList<LinkedList<String> > listOfListOfString = new LinkedList<LinkedList<String> >();
        listOfListOfString.add(stringList);
        String myString2 = listOfListOfString.iterator().next().iterator().next();
        System.out.println(myString2);
    }
}