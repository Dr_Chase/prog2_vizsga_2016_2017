package com.drchase.vizsga.fotetel7;

interface Collection<A> {
    public void add(A x);
    public Iterator<A> iterator();
}