package com.drchase.vizsga.fotetel7;

interface Iterator<A>   {
    public A next();
    public boolean hasNext();
}