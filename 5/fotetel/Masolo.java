/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.drchase.vizsga.fotetel5;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author Dr.Chase
 */
public class Masolo {

    /**
     * @param args the command line arguments
     * 
     */
    final static int BLOKK_MERET = 1;
    
    static void masol(InputStream in, OutputStream out) throws IOException  {
        byte[] b = new byte[BLOKK_MERET];
        int hossz;
        try {
            while ( (hossz = in.read(b)) == BLOKK_MERET) out.write(b);
            if( hossz != -1 ) out.write(b,0,hossz);
            
        }
        finally { out.flush();  }
    }
    
    
    
    
}
