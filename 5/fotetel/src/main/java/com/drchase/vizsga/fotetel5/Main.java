package com.drchase.vizsga.fotetel5;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Main {
    public static void main(String[] args) throws java.io.IOException {
        // TODO code application logic here
        java.io.FileInputStream fin = new java.io.FileInputStream("Masolo.java");
        try {
            Masolo.masol(fin, System.out);
        }
        catch (IOException ex)   {
            System.err.println("Input file can't be found!!!");
            ex.printStackTrace();
        } finally   {
            fin.close();    // garantált lezárás
        }
    }
}