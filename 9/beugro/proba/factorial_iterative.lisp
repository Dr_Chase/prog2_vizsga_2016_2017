(defun fact-iter (n)
  (iter 1 1 n))
(defun iter (product count n)
  (if (> count n)
      product
(iter (* product count) (1+ count) n)))