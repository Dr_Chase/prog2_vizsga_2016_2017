;;FACTORIAL OF A NUMBER USING RECURSION AND ITERATION
(defun factorial(n)
    (format t "~&<<<Finding factorial of ~D>>>~&1.Iterative method~&2.Recursive method~&Enter your choice:" n)
    (setf x (read))
    (if (= x 1) (format t "factorial = ~D (using iteration)" (fact1 n)))
    (if (= x 2) (format t "factorial = ~D (using recursion)" (fact2 n)))
)
(defun fact1(n) ;;function for iterative method
    (setf f 1) 
    (do ((i n (- i 1))) ((= i 1)) 
        (setf f (* f i)) 
    ) 
f) 
(defun fact2(n) ;;function for recursive method
    (if (= n 0) 1 
        (* n (fact2(- n 1))) 
    ) 
)