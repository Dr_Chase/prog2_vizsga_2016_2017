package com.drchase.vizsga.fotetel9;

public enum Bolygo  {
    Merkur (3.303e+23, 2.4397e6),
    Venusz (4.869e+24, 6.0518e6),
    Fold   (5.976e+24, 6.37814e6),
    Mars   (6.421e+23, 3.3972e6),
    Jupiter(5.688e+26, 6.0268e7),
    Szaturnusz(5.688e+26, 6.0268e7),
    Uranusz(8.686e+25, 2.5559e7),
    Neptunusz(1.024e+26, 2.4746e7);
    private final double tomeg;     // kilogram
    private final double sugar;     // metres
    Bolygo(double tomeg, double sugar)  {
        this.tomeg = tomeg;
        this.sugar = sugar;
    }
    public double tomeg()   {
        return tomeg;
    }
    public double sugar()   {
        return sugar;
    }
    public static final double G = 6.67300E-11; // gravitációs állandó
    public double felsziniGravitacio()  {
        return G * tomeg / (sugar * sugar);
    }
    public double felsziniSuly(double tomeg)    {
        return tomeg * felsziniGravitacio();
    }
}