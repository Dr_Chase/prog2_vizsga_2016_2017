package com.drchase.vizsga.fotetel9;

public class Main {
    public static void main(String[] args) {
        double foldiSuly = Double.parseDouble(args[0]);
        double tomeg = foldiSuly / Bolygo.Fold.felsziniGravitacio();
        for (Bolygo p : Bolygo.values())    {

            System.out.printf( "A súlya a %s bolygón %f%n", p, p.felsziniSuly(tomeg));
        }
    }
}