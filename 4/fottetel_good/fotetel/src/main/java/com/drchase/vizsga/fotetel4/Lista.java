package com.drchase.vizsga.fotetel4;

import java.util.Iterator;
import java.util.NoSuchElementException;



// Javakönyv 350. oldal

public class Lista  {
    private static class Elem   {
        Object adat;
        Elem elozo, kovetkezo;
        Elem (Object adat, Elem elozo, Elem kovetkezo)  {
            this.adat = adat;
            this.elozo = elozo;
            this.kovetkezo = kovetkezo;
        }
        //kiegészítés
        @Override
        public String toString()   {
            return adat.toString();
        }
    }
    public Iterator felsorol () {
        return new Felsorolo(this);
    }

    //kiegészítés
    @Override
    public String toString()   {
        Felsorolo felsorolas = new Felsorolo(this);
        StringBuilder sb = new StringBuilder();
        Object o;
        sb.append("==============================\n");
        while(felsorolas.hasNext()) {
            o = felsorolas.next();
            sb.append(o.toString());
            sb.append("\n");
        }
        sb.append("==============================\n");
        return sb.toString();
    }
    private static class Felsorolo implements Iterator  {
        final Lista lista;
        Elem aktualis;
        Elem torolheto;
        Felsorolo(Lista lista)  {
            this.lista = lista;
            aktualis = lista.elso;
        }
        public boolean hasNext()    {
            return aktualis != null;
        }
        public Object next () throws NoSuchElementException {
            if (aktualis == null) throw new NoSuchElementException();
            torolheto = aktualis;
            aktualis = aktualis.kovetkezo;
            return torolheto.adat;
        }
        public void remove() throws IllegalStateException   {
            if (torolheto == null) throw new IllegalStateException();
            lista.torol(torolheto);
            torolheto = null;
        }
    }

    private Elem elso;
    public void beszur (Object adat)    {
        elso = new Elem ( adat, null, elso);
        if (elso.kovetkezo != null) {
            elso.kovetkezo.elozo = elso;
        }
    }
    public void torol (Object adat) {
        Elem elem = keres(adat);
        if (elem != null)   {
            torol(elem);    
        }
    }
    private void torol (Elem elem)  {
        if(elem == elso)    {
            elso = elem.kovetkezo;
        }
        if (elem.elozo != null) {
            elem.elozo.kovetkezo = elem.kovetkezo;
        }
        if (elem.kovetkezo != null) {
            elem.kovetkezo.elozo = elem.elozo;
        }
    }
    private Elem keres (Object adat)    {

        for (Elem elem = elso; elem != null; elem = elem.kovetkezo) {
            if(elem.adat.equals(adat))  {
                return elem;
            }
        }
        return null;
        
    }
}