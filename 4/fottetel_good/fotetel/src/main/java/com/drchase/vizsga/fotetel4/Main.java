package com.drchase.vizsga.fotetel4;

public class Main {
    public static void main(String[] args) {
       Lista myLista = new Lista();
       String string1 = "Van cicátok meg kutyátok?";
       String string2 = "kutya";
       String string3 = "cica";
       String string4 = "kakas";
       String string5 = "rémszarvas";

       myLista.beszur(string1);
       myLista.beszur(string2);
       myLista.beszur(string3);
       myLista.beszur(string4);
       myLista.beszur(string5);

       System.out.println(myLista);

       myLista.torol("cica");
       System.out.println(myLista);
    }
}