package com.drchase.vizsga.fotetel7;


// Javakönyv 350. oldal

public class Verem <A> {
    public Verem()  {
        v = (A[]) new Object[max_size];
        sz = 0;
    }
    public void push (A x) throws Exception {
        if ( sz < max_size )    {
            v[sz++] = x;
        }
        else    {
            throw new Exception("full");
        }
        public A pop() throws Exception {
            if ( sz > 0 )   {
                return v[--sz];
            }
            else    {
                throw new Exception ("empty");
            }
        }
        public int size()   {
            return sz;
        }
        private A[] v;
        private int sz;
        private static int max_size = 1000;
    }


}